package org.dhbw.mosbach.inf14b.bstu.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@XmlRootElement
public class SportsLeaguesUser
{
	private long id;
	private String username;
	private String password;
	private List<League> favouriteLeagues;
	private List<LeagueTeam> favouriteTeams;

	public SportsLeaguesUser() {
		super();
		favouriteLeagues = new ArrayList<>();
		favouriteTeams = new ArrayList<>();
	}

	@Id
	@GeneratedValue
	@XmlTransient
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Column(nullable = false, length = 64, unique = true)
	@XmlAttribute(required = true)
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@Column(nullable = false, length = 64)
	@XmlAttribute(required = true)
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@ManyToMany(targetEntity = League.class, fetch=FetchType.EAGER)
	public List<League> getFavouriteLeagues() {
		return favouriteLeagues;
	}

	public void setFavouriteLeagues(List<League> favouriteLeagues) {
		this.favouriteLeagues = favouriteLeagues;
	}

	@ManyToMany(targetEntity = LeagueTeam.class, fetch=FetchType.EAGER)
	@JoinTable(name="USER_TEAM",
		       joinColumns=@JoinColumn(name="USR_ID", referencedColumnName="id", foreignKey = @ForeignKey(name = "fk_user")),
		       inverseJoinColumns=@JoinColumn(name="TEAM_ID", referencedColumnName="teamID"))
	public List<LeagueTeam> getFavouriteTeams() {
		return favouriteTeams;
	}

	public void setFavouriteTeams(List<LeagueTeam> favouriteTeams) {
		this.favouriteTeams = favouriteTeams;
	}
}
