package org.dhbw.mosbach.inf14b.bstu.services;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import org.dhbw.mosbach.inf14b.bstu.db.Database;
import org.dhbw.mosbach.inf14b.bstu.model.League;
import org.dhbw.mosbach.inf14b.bstu.model.LeagueTeam;

import de.msiggi.sportsdata.webservices.Sportsdata;
import de.msiggi.sportsdata.webservices.SportsdataSoap;

//initialize database by fetching leagues and teams from the corresponding web services of OpenLigaDB
//persist them via JPA eclipseLink
public class InitDB {
	
  public void init() {
		
		EntityManager em = Database.getEntityManager();
		final EntityTransaction transaction = em.getTransaction();
		transaction.begin();
		em.createQuery("delete from SportsLeaguesUser").executeUpdate();
		em.createQuery("delete from LeagueTeam").executeUpdate();
		em.createQuery("delete from League").executeUpdate();
		
		Sportsdata sportsdata = new Sportsdata();
		SportsdataSoap service = sportsdata.getSportsdataSoap();
		
		List<String> leagueShortcuts = new ArrayList<>();
		leagueShortcuts.add("bl1");
		leagueShortcuts.add("bl2");
		leagueShortcuts.add("bl3");
		leagueShortcuts.add("PL");
		leagueShortcuts.add("PD");
		leagueShortcuts.add("SA");
		leagueShortcuts.add("ATBL1");
		List<de.msiggi.sportsdata.webservices.Team> teamList;
		List<de.msiggi.sportsdata.webservices.League> leagues = service.getAvailLeaguesBySports(1).getLeague();
		
		for(de.msiggi.sportsdata.webservices.League l : leagues) {
			
			//persisting only leagues of the current season
			if (leagueShortcuts.contains(l.getLeagueShortcut()) && l.getLeagueSaison().equals("2015")) {
				
				League league = new League();
				league.setLeagueID(l.getLeagueID());
				league.setLeagueName(l.getLeagueName());
				league.setLeagueSaison(l.getLeagueSaison());
				league.setLeagueShortcut(l.getLeagueShortcut());
				league.setLeagueSportID(l.getLeagueSportID());
				em.persist(league);
				
				//list of teams playing within that league
				teamList = service.getTeamsByLeagueSaison(league.getLeagueShortcut(), "2015").getTeam();
				
				for (final de.msiggi.sportsdata.webservices.Team team : teamList)
				{	
						LeagueTeam persistTeam = new LeagueTeam();
						persistTeam.setTeamID(team.getTeamID());
						persistTeam.setTeamName(team.getTeamName());
						persistTeam.setLeague(league);
						em.persist(persistTeam);	
				}
			}
		}
		transaction.commit();
   } 
}
