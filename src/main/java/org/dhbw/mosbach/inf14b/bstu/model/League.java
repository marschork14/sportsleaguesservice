package org.dhbw.mosbach.inf14b.bstu.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement
public class League
{
	private int leagueID;
	private int leagueSportID;
	private String leagueName;
	private String leagueShortcut;
	private String leagueSaison;


	public League() {
		super();
	}
 
	@Id
	@XmlAttribute(required = true)
	public int getLeagueID() {
		return leagueID;
	}

	public void setLeagueID(int leagueID) {
		this.leagueID = leagueID;
	}

	@Column(nullable = false)
	@XmlAttribute(required = true)
	public int getLeagueSportID() {
		return leagueSportID;
	}

	public void setLeagueSportID(int leagueSportId) {
		this.leagueSportID = leagueSportId;
	}

	@Column(nullable = false, length = 64)
	@XmlAttribute(required = true)
	public String getLeagueName() {
		return leagueName;
	}

	public void setLeagueName(String leagueName) {
		this.leagueName = leagueName;
	}

	@Column(nullable = false, length = 16, unique = true)
	@XmlAttribute(required = true)
	public String getLeagueShortcut() {
		return leagueShortcut;
	}

	public void setLeagueShortcut(String leagueShortCut) {
		this.leagueShortcut = leagueShortCut;
	}

	@Column(nullable = false, length = 64)
	@XmlAttribute(required = true)
	public String getLeagueSaison() {
		return leagueSaison;
	}

	public void setLeagueSaison(String leagueSaison) {
		this.leagueSaison = leagueSaison;
	}
}
