package org.dhbw.mosbach.inf14b.bstu.exceptions;

import java.lang.Exception;

public class NoAuthorizationException extends Exception {

	private static final long serialVersionUID = 1L;

	public NoAuthorizationException() {
		super("You're not authorized for using this service! Please, login first...");
	}
}
