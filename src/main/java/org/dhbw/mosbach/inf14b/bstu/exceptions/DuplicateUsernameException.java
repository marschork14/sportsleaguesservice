package org.dhbw.mosbach.inf14b.bstu.exceptions;

public class DuplicateUsernameException extends Exception {

	private static final long serialVersionUID = 1L;

	public DuplicateUsernameException() {
		super("Username already exists! Choose another one...");
	}
}
