package org.dhbw.mosbach.inf14b.bstu.exceptions;

import java.lang.Exception;

public class AuthenticationFailedException extends Exception {

	private static final long serialVersionUID = 1L;

	public AuthenticationFailedException() {
		super("Authentication failed! Wrong username or password...");
	}
}
