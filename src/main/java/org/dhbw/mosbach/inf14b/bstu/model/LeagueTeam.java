package org.dhbw.mosbach.inf14b.bstu.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement
public class LeagueTeam {
	
	private int teamID;
	private String teamName;
	private League league;
	
	public LeagueTeam() {
		super();
	}
 
	@Id
	@XmlAttribute(required = true)
	public int getTeamID() {
		return teamID;
	}

	public void setTeamID(int teamID) {
		this.teamID = teamID;
	}


	@Column(nullable = false, length = 64)
	@XmlAttribute(required = true)
	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(String leagueName) {
		this.teamName = leagueName;
	}

	
	@ManyToOne(optional=false, fetch=FetchType.EAGER)
	public League getLeague() {
		return league;
	}

	public void setLeague(League league) {
		this.league = league;
	}
}
