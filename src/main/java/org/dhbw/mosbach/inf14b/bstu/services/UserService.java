package org.dhbw.mosbach.inf14b.bstu.services;

import java.util.List;

import javax.annotation.Resource;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletRequest;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;

import org.dhbw.mosbach.inf14b.bstu.db.Database;
import org.dhbw.mosbach.inf14b.bstu.exceptions.AuthenticationFailedException;
import org.dhbw.mosbach.inf14b.bstu.exceptions.DuplicateUsernameException;
import org.dhbw.mosbach.inf14b.bstu.exceptions.NoAuthorizationException;
import org.dhbw.mosbach.inf14b.bstu.model.League;
import org.dhbw.mosbach.inf14b.bstu.model.SportsLeaguesUser;
import org.dhbw.mosbach.inf14b.bstu.model.LeagueTeam;

@WebService
public class UserService
{	
	@Resource
	private WebServiceContext wsContext;
	
	//create a new user within the system
	public void addSportsLeaguesUser(@WebParam(name = "username") final String username, @WebParam(name = "password") final String password) throws DuplicateUsernameException, AuthenticationFailedException
	{	
		EntityManager em = Database.getEntityManager();
		
		@SuppressWarnings("unchecked")
		List<SportsLeaguesUser> sportsLeaguesUserToFind = em.createQuery("FROM SportsLeaguesUser s WHERE s.username = :username")
											   .setParameter("username", username).getResultList();
		
		//username already exists
		if(sportsLeaguesUserToFind.size() != 0) {
			throw new DuplicateUsernameException();
		}

		//create new user and persist
		SportsLeaguesUser newUser = new SportsLeaguesUser();
		newUser.setPassword(password);
		newUser.setUsername(username);
		
		EntityTransaction transaction = em.getTransaction();
		transaction.begin();
		em.persist(newUser);
		transaction.commit();
		em.close();
	}
	
	//user authentication
	public SportsLeaguesUser login(@WebParam(name = "username") final String username, @WebParam(name = "password") final String password) throws AuthenticationFailedException {
		
		SportsLeaguesUser sportsLeaguesUser = isUserSessionOpen();
		//user is not logged in already
		if(sportsLeaguesUser == null) {

			EntityManager em = Database.getEntityManager();
		
			@SuppressWarnings("unchecked")
			List<SportsLeaguesUser> sportsLeaguesUserToFind = em.createQuery("FROM SportsLeaguesUser s WHERE s.username = :username AND s.password = :password")
												   .setParameter("username", username).setParameter("password", password).getResultList();
		
			em.close();
			// user exists
			if(sportsLeaguesUserToFind.size() > 0) {
				openSession(sportsLeaguesUserToFind.get(0));
				return sportsLeaguesUserToFind.get(0);	
			}
			
			throw new AuthenticationFailedException();
		}
		
		return sportsLeaguesUser;
	}
	
	/*
	public boolean removeSportsLeaguesUser(@WebParam(name = "username") final String username, @WebParam(name = "password") final String password) {
		
		logger.log(Level.INFO, "Call to removeSportsLeaguesUser");
				
		SportsLeaguesUser sportsLeaguesUserToRemove = login(username, password);
		//no such user exists
		if(sportsLeaguesUserToRemove == null) {
			return false;
		}
		
		EntityManager em = Database.getEntityManager();
		EntityTransaction transaction = em.getTransaction();
		
		transaction.begin();
		sportsLeaguesUserToRemove = em.find(SportsLeaguesUser.class, sportsLeaguesUserToRemove.getId());
		em.remove(sportsLeaguesUserToRemove);
		transaction.commit();
		em.close();
		return true;
	}
	*/
	// adding a league to the users favorites
	public void addLeagueToUser(@WebParam(name = "leagueId") final int leagueId) throws NoAuthorizationException {
		
		// only possible if the user is logged in
		SportsLeaguesUser user = isUserSessionOpen();
		if(user == null) {
			throw new NoAuthorizationException();
		}

		EntityManager em = Database.getEntityManager();
		EntityTransaction transaction = em.getTransaction();
		transaction.begin();
		
		League leagueToFind = (League) em.find(League.class, leagueId);
		
		List<League> favouriteLeagues = user.getFavouriteLeagues();
		if(!favouriteLeagues.contains(leagueToFind)) {
			favouriteLeagues.add(leagueToFind);
		}
		
		em.merge(user);
		transaction.commit();
		em.close();		
	}
	
	// removing a league from the users favorites
	public void removeLeagueFromUser(@WebParam(name = "leagueId") final int leagueId) throws NoAuthorizationException {
		
		
		// only possible if user is logged in
		SportsLeaguesUser user = isUserSessionOpen();
		if(user == null) {
			throw new NoAuthorizationException();
		}

		EntityManager em = Database.getEntityManager();
		EntityTransaction transaction = em.getTransaction();
		transaction.begin();
		
		List<League> favouriteLeagues = user.getFavouriteLeagues();
		
		for(int i = 0; i < favouriteLeagues.size(); i++) {
			
			if(favouriteLeagues.get(i).getLeagueID()== leagueId) {
				favouriteLeagues.remove(i);
				break;
			}
		}
		
		em.merge(user);
		transaction.commit();
		em.close();	
	}
	
	// adding a team to the users favorites
	public void addTeamToUser(@WebParam(name = "teamId") final int teamId) throws NoAuthorizationException {
		
		// only possible if the user is logged in
		SportsLeaguesUser user = isUserSessionOpen();
		if(user == null) {
			throw new NoAuthorizationException();
		}

		EntityManager em = Database.getEntityManager();
		EntityTransaction transaction = em.getTransaction();
		transaction.begin();
		
		LeagueTeam teamToFind = (LeagueTeam) em.find(LeagueTeam.class, teamId);
		
		List<LeagueTeam> favouriteTeams = user.getFavouriteTeams();
		if(!favouriteTeams.contains(teamToFind)) {
			favouriteTeams.add(teamToFind);
		}
		
		em.merge(user);
		transaction.commit();
		em.close();		
	}
	
	// removing a team from the users favorites
	public void removeTeamFromUser(@WebParam(name = "teamId") final int teamId) throws NoAuthorizationException {
		
		// only possible if the user is logged in
		SportsLeaguesUser user = isUserSessionOpen();
		if(user == null) {
			throw new NoAuthorizationException(); 
		}

		EntityManager em = Database.getEntityManager();
		EntityTransaction transaction = em.getTransaction();
		transaction.begin();
		
		List<LeagueTeam> favouriteTeams = user.getFavouriteTeams();
		
		for(int i = 0; i < favouriteTeams.size(); i++) {
			
			if(favouriteTeams.get(i).getTeamID()== teamId) {
				favouriteTeams.remove(i);
				break;
			}
		}
		
		em.merge(user);
		transaction.commit();
		em.close();	
	}
	
	// closing session by a service call
	public void logout() {
		closeSession();
	}
	
	// check if there is already a open session for that user 
	private SportsLeaguesUser isUserSessionOpen() {
		 
		MessageContext messageContext = wsContext.getMessageContext();
		HttpSession httpSession = ((HttpServletRequest)messageContext.get(MessageContext.SERVLET_REQUEST)).getSession();
		if(httpSession.getAttribute("user") != null) {
			return (SportsLeaguesUser) httpSession.getAttribute("user");
		}
		return null;
	}
	
	// open a user session by linking the user object to a session cookie
	private void openSession(SportsLeaguesUser sportsLeaguesUser) {
		
		MessageContext messageContext = wsContext.getMessageContext();
		HttpSession httpSession = ((HttpServletRequest)messageContext.get(MessageContext.SERVLET_REQUEST)).getSession();
		httpSession.setAttribute("user", sportsLeaguesUser);
		
	}
	
	// invalidate session
	private void closeSession() {
		
		MessageContext messageContext = wsContext.getMessageContext();
		HttpSession httpSession   = ((HttpServletRequest)messageContext.get(MessageContext.SERVLET_REQUEST)).getSession();
		httpSession.invalidate();
	}
}
