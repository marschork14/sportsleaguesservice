package org.dhbw.mosbach.inf14b.bstu.services;

import java.util.ArrayList;
import java.util.List;

import javax.jws.WebParam;
import javax.jws.WebService;
import javax.persistence.EntityManager;

import org.dhbw.mosbach.inf14b.bstu.db.Database;
import org.dhbw.mosbach.inf14b.bstu.model.League;
import org.dhbw.mosbach.inf14b.bstu.model.LeagueTeam;

import de.msiggi.sportsdata.webservices.GoalGetter;
import de.msiggi.sportsdata.webservices.Group;
import de.msiggi.sportsdata.webservices.Matchdata;
import de.msiggi.sportsdata.webservices.Sportsdata;
import de.msiggi.sportsdata.webservices.SportsdataSoap;


@WebService
public class SportsLeaguesService
{
	public List<League> getAllLeagues() {

		EntityManager em = Database.getEntityManager();

		@SuppressWarnings("unchecked")
		List<League> leagues = (List<League>) em.createQuery("FROM League l").getResultList();
		
		em.close();
		return leagues;
	}
	
	public List<LeagueTeam> getAllTeams() {

		EntityManager em = Database.getEntityManager();

		@SuppressWarnings("unchecked")
		List<LeagueTeam> leagueTeams = (List<LeagueTeam>) em.createQuery("FROM LeagueTeam t").getResultList();
		
		em.close();
		return leagueTeams;
	}
	
	public List<LeagueTeam> getAllTeamsByLeagueId(@WebParam(name="leagueId") final int leagueId) {
		
		EntityManager em = Database.getEntityManager();

		@SuppressWarnings("unchecked")
		List<LeagueTeam> leagueTeams = (List<LeagueTeam>) em.createQuery("FROM LeagueTeam t").getResultList();
		List<LeagueTeam> resultTeams = new ArrayList<>();
		LeagueTeam tmp;
		for(int i = 0; i < leagueTeams.size(); i++) {
			tmp = leagueTeams.get(i);
			if(tmp.getLeague().getLeagueID() == leagueId) {
				resultTeams.add(tmp);
			}
		}
		em.close();
		return resultTeams;
	}
	
	public List<Matchdata> getMatchDataByGroupIDAndLeagueShortCut(@WebParam(name = "groupID") final int groupID, @WebParam(name = "leagueShortcut") final String leagueShortcut) {
		
		SportsdataSoap service = buildService();
		return service.getMatchdataByGroupLeagueSaison(groupID, leagueShortcut, "2015").getMatchdata();
	}
	
	public List<GoalGetter> getGoalGetterByLeagueShortCut(@WebParam(name = "leagueShortcut") final String leagueShortcut) {
		
		SportsdataSoap service = buildService();
		return service.getGoalGettersByLeagueSaison(leagueShortcut, "2015").getGoalGetter();
	}
	
	public Matchdata getNextMatchLeagueTeam(@WebParam(name = "leagueID") final int leagueID, @WebParam(name = "teamID") final int teamID) {
		
		SportsdataSoap service = buildService();
		return service.getNextMatchByLeagueTeam(leagueID, teamID);
	}
	
	public Matchdata getLastMatchLeagueTeam(@WebParam(name = "leagueID") final int leagueID, @WebParam(name = "teamID") final int teamID) {
		
		SportsdataSoap service = buildService();
		return service.getLastMatchByLeagueTeam(leagueID, teamID);
	}
	
	public List<Group> getGroupsByLeagueShortcut(@WebParam(name = "leagueShortcut") final String leagueShortcut) {
		
		SportsdataSoap service = buildService();
		return service.getAvailGroups(leagueShortcut, "2015").getGroup();
	}
	
	private SportsdataSoap buildService() {
		
		Sportsdata sportsdata = new Sportsdata();
		SportsdataSoap service = sportsdata.getSportsdataSoap();
		return service;
	}
}
