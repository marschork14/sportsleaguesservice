
package de.msiggi.sportsdata.webservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetMatchdataByGroupLeagueSaisonResult" type="{http://msiggi.de/Sportsdata/Webservices}ArrayOfMatchdata" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getMatchdataByGroupLeagueSaisonResult"
})
@XmlRootElement(name = "GetMatchdataByGroupLeagueSaisonResponse")
public class GetMatchdataByGroupLeagueSaisonResponse {

    @XmlElement(name = "GetMatchdataByGroupLeagueSaisonResult")
    protected ArrayOfMatchdata getMatchdataByGroupLeagueSaisonResult;

    /**
     * Ruft den Wert der getMatchdataByGroupLeagueSaisonResult-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfMatchdata }
     *     
     */
    public ArrayOfMatchdata getGetMatchdataByGroupLeagueSaisonResult() {
        return getMatchdataByGroupLeagueSaisonResult;
    }

    /**
     * Legt den Wert der getMatchdataByGroupLeagueSaisonResult-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfMatchdata }
     *     
     */
    public void setGetMatchdataByGroupLeagueSaisonResult(ArrayOfMatchdata value) {
        this.getMatchdataByGroupLeagueSaisonResult = value;
    }

}
