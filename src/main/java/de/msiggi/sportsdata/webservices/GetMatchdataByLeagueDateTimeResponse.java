
package de.msiggi.sportsdata.webservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetMatchdataByLeagueDateTimeResult" type="{http://msiggi.de/Sportsdata/Webservices}ArrayOfMatchdata" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getMatchdataByLeagueDateTimeResult"
})
@XmlRootElement(name = "GetMatchdataByLeagueDateTimeResponse")
public class GetMatchdataByLeagueDateTimeResponse {

    @XmlElement(name = "GetMatchdataByLeagueDateTimeResult")
    protected ArrayOfMatchdata getMatchdataByLeagueDateTimeResult;

    /**
     * Ruft den Wert der getMatchdataByLeagueDateTimeResult-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfMatchdata }
     *     
     */
    public ArrayOfMatchdata getGetMatchdataByLeagueDateTimeResult() {
        return getMatchdataByLeagueDateTimeResult;
    }

    /**
     * Legt den Wert der getMatchdataByLeagueDateTimeResult-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfMatchdata }
     *     
     */
    public void setGetMatchdataByLeagueDateTimeResult(ArrayOfMatchdata value) {
        this.getMatchdataByLeagueDateTimeResult = value;
    }

}
