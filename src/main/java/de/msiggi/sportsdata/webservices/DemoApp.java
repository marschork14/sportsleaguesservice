package de.msiggi.sportsdata.webservices;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


public class DemoApp {

		public static void main(String[] args) {
		// Verbindung zum Webservice herstellen
		Sportsdata sportsdata = new Sportsdata();
		// Serviceobject erstellen um die Daten abzufragen
		SportsdataSoap service = sportsdata.getSportsdataSoap();
		// eine Liste der verf�gbaren Ligen abfragen
		List<String> leagueShortcuts = new ArrayList<>();
		leagueShortcuts.add("bl1");
		leagueShortcuts.add("bl2");
		leagueShortcuts.add("bl3");
		leagueShortcuts.add("PL");
		leagueShortcuts.add("PD");
		leagueShortcuts.add("SA");
		leagueShortcuts.add("ATBL1");
			
		
		List<League> leagues = service.getAvailLeaguesBySports(1).getLeague();
			for(League l : leagues){
				if (leagueShortcuts.contains(l.getLeagueShortcut()) && l.getLeagueSaison().equals("2015")) {
					System.out.println(l.getLeagueName() + " || " + l.getLeagueShortcut());
				}
		}
			
		
	}
}
