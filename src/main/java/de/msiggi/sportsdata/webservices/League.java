package de.msiggi.sportsdata.webservices;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "League", propOrder = {
    "leagueID",
    "leagueSportID",
    "leagueName",
    "leagueShortcut",
    "leagueSaison"
})
public class League
{
	private int leagueID;
	private int leagueSportID;
	private String leagueName;
	private String leagueShortcut;
	private String leagueSaison;


	public League() {
		super();
	}
 
	@Id
	public int getLeagueID() {
		return leagueID;
	}

	public void setLeagueID(int leagueID) {
		this.leagueID = leagueID;
	}

	@Column(nullable = false)
	public int getLeagueSportID() {
		return leagueSportID;
	}

	public void setLeagueSportID(int leagueSportId) {
		this.leagueSportID = leagueSportId;
	}

	@Column(nullable = false, length = 256)
	public String getLeagueName() {
		return leagueName;
	}

	public void setLeagueName(String leagueName) {
		this.leagueName = leagueName;
	}

	@Column(nullable = false, length = 128, unique = true)
	public String getLeagueShortcut() {
		return leagueShortcut;
	}

	public void setLeagueShortcut(String leagueShortCut) {
		this.leagueShortcut = leagueShortCut;
	}

	@Column(nullable = false, length = 256)
	public String getLeagueSaison() {
		return leagueSaison;
	}

	public void setLeagueSaison(String leagueSaison) {
		this.leagueSaison = leagueSaison;
	}
}
