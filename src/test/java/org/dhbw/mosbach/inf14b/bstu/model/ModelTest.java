package org.dhbw.mosbach.inf14b.bstu.model;

import static org.junit.Assert.assertEquals;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import org.dhbw.mosbach.inf14b.bstu.db.Database;
import org.dhbw.mosbach.inf14b.bstu.model.SportsLeaguesUser;
import org.junit.Before;
import org.junit.Test;

public class ModelTest
{
	private static final String USER_NAME = "dampfHans";
	private static final String USER_PASSWORD = "123456";
	
	private static final int LEAGUE_ID = 1;
	private static final int LEAGUE_SPORT_ID = 2;
	private static final String LEAGUE_NAME = "1. Bundesliga";
	private static final String LEAGUE_SHORT_CUT = "bl1";
	private static final String LEAGUE_SAISON = "2015/2016";
	
	private static final int TEAM_ID = 10;
	private static final String TEAM_NAME = "VfB Stuttgart e.V.";

	@Before
	public void init()
	{
		Database.enableTesting();
	}

	@Test
	public void testPersistUser()
	{
		final EntityManager em = Database.getEntityManager();

		final SportsLeaguesUser user = new SportsLeaguesUser();
		user.setUsername(USER_NAME);
		user.setPassword(USER_PASSWORD);

		final EntityTransaction transaction = em.getTransaction();
		transaction.begin();
		em.createQuery("delete from SportsLeaguesUser").executeUpdate();
		em.persist(user);
		transaction.commit();

		@SuppressWarnings("unchecked")
		final List<SportsLeaguesUser> resultList = em.createQuery("from SportsLeaguesUser a").getResultList();
		for (final SportsLeaguesUser a : resultList)
		{
			System.out.println(a.getUsername());
			assertEquals(a.getUsername(), USER_NAME);
			System.out.println(a.getPassword());
			assertEquals(a.getPassword(), USER_PASSWORD);
		}
		assertEquals(resultList.size(), 1);

		em.close();
	}
	
	@Test
	public void testPersistTeamAndLeague()
	{
		final EntityManager em = Database.getEntityManager();

		final League league = new League();
		league.setLeagueID(LEAGUE_ID);
		league.setLeagueSportID(LEAGUE_SPORT_ID);
		league.setLeagueName(LEAGUE_NAME);
		league.setLeagueShortcut(LEAGUE_SHORT_CUT);
		league.setLeagueSaison(LEAGUE_SAISON);

		final EntityTransaction transaction = em.getTransaction();
		transaction.begin();
		em.createQuery("delete from SportsLeaguesUser").executeUpdate();
		em.createQuery("delete from LeagueTeam").executeUpdate();
		em.createQuery("delete from League").executeUpdate();
		
		em.persist(league);

		@SuppressWarnings("unchecked")
		final List<League> resultList = em.createQuery("from League a").getResultList();
		for (final League a : resultList)
		{
			System.out.println(a.getLeagueID());
			assertEquals(a.getLeagueID(), LEAGUE_ID);
			System.out.println(a.getLeagueID());
			assertEquals(a.getLeagueSportID(), LEAGUE_SPORT_ID);
			System.out.println(a.getLeagueName());
			assertEquals(a.getLeagueName(), LEAGUE_NAME);
			System.out.println(a.getLeagueShortcut());
			assertEquals(a.getLeagueShortcut(), LEAGUE_SHORT_CUT);
			System.out.println(a.getLeagueSaison());
			assertEquals(a.getLeagueSaison(), LEAGUE_SAISON);
	
		}
		assertEquals(resultList.size(), 1);
		
		final LeagueTeam leagueTeam = new LeagueTeam();
		leagueTeam.setTeamID(TEAM_ID);
		leagueTeam.setTeamName(TEAM_NAME);
		leagueTeam.setLeague(league);
		
		em.persist(leagueTeam);

		@SuppressWarnings("unchecked")
		final List<LeagueTeam> result = em.createQuery("from LeagueTeam a").getResultList();
		for (final LeagueTeam a : result)
		{
			System.out.println(a.getTeamID());
			assertEquals(a.getTeamID(), TEAM_ID);
			System.out.println(a.getTeamName());
			assertEquals(a.getTeamName(), TEAM_NAME);
			System.out.println(a.getLeague().getLeagueName());
			assertEquals(a.getLeague().getLeagueName(), LEAGUE_NAME);
		}
		assertEquals(result.size(), 1);

		transaction.commit();
		em.close();
	}
}
